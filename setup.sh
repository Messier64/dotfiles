read -p 'Password: ' pass

echo $pass | sudo -kS pacman -Syu nvidia-prime nvidia nvidia-utils nvidia-settings amd-ucode xf86-video-amdgpu vulkan-radeon polybar sxhkd rofi kitty chromium playerctl bc firefox xorg-xinit xorg-server xorg-xset xorg-xsetroot feh pulseaudio pavucontrol lightdm nautilus networkmanager papirus-icon-theme ttf-font-awesome ttf-hack xclip python2 python3 xsettingsd discord lxappearance steam mpv bspwm inotify-tools lightdm-slick-greeter lib32-mesa --noconfirm

git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg
echo $pass | sudo -kS pacman -U *.pkg.tar.* --noconfirm
cd ..
echo $pass | sudo -kS rm -rf paru-bin

echo $pass | sudo -S paru -Ss polybar
paru -S papirus-folders zoom wpgtk ttf-twemoji spotify spotify-adblock-git plymouth --noconfirm


ln -sfv $HOME/.dotfiles/.config/bspwm $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/sxhkd $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/kitty $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/rofi $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/polybar $HOME/.config/
ln -sfv $HOME/.dotfiles/.scripts $HOME/
ln -sfv $HOME/.dotfiles/.xprofile $HOME/
ln -sfv $HOME/.dotfiles/.wallpapers $HOME/
echo $pass | sudo -kS cp $HOME/.dotfiles/.wallpapers/lockscreen.jpg /etc/lightdm/
echo $pass | sudo -kS vim /etc/lightdm/lightdm.conf

cat <<EOT >> /etc/lightdm/slick-greeter.conf
[Greeter]
background=/etc/lightdm/lockscreen.jpg
user=messier
icon-theme-name=Adwaita
EOT

echo $pass | sudo -kS sed -i 's/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/g' /etc/mkinitcpio.conf
echo $pass | sudo -kS mkinitcpio -P
echo $pass | sudo -kS mkdir -p /etc/pacman.d/hooks
echo $pass | sudo -kS cp -r $HOME/.$USER/.nvidia/nvidia.hook /etc/pacman.d/hooks/
echo $pass | sudo -kS cp -r $HOME/.$USER/.nvidia/70-nvidia.rules /etc/udev/rules.d/

wpg-install.sh -g
wpg -a $HOME/.wallpapers/901676.jpg
wpg -s 901676.jpg
echo $pass | sudo -kS systemctl enable lightdm

echo $pass | sudo -kS cp $HOME/.$USER/nobeep.conf /etc/modprobe.d/

sh $HOME/.$USER/.scripts/dns
reboot
