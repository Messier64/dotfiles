[colors]
background = ${xrdb:color0:#222}
foreground = ${xrdb:color7:#222}
foreground-alt = ${xrdb:color7:#222}
primary = ${xrdb:color1:#222}
secondary = ${xrdb:color2:#222}
alert = ${xrdb:color3:#222}

[bar/top]
width = 98%
height = 4%
offset-x = 1%
offset-y = 1%
radius = 0
fixed-center = true
enable-ipc = true
bottom = false 
background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 1
padding-right = 1

module-margin-left = 1
module-margin-right = 1

font-0 = Hack:pixelsize=11:antialias=true;2
font-1 = FontAwesome6Free:style=Solid:pixelsize=11:antialias=true;2
font-2 = FontAwesome6Brands:style=Solid:pixelsize=11:antialias=true;2

modules-left = powermenu bspwm 
modules-center = time
modules-right =  updates battery backlight microphone pulseaudio memory cpu

tray-position = right 
tray-padding = 1

wm-restack = bspwm

;override-redirect = true

cursor-click = pointer
cursor-scroll = ns-resize

;user modules

[module/updates]
type = custom/script
exec = ~/.scripts/updates
interval = 600

[module/microphone]
type = custom/script
exec = ~/.scripts/microphone
tail = true
click-left = ~/.scripts/microphone --toggle &
scroll-up = ~/.scripts/pulseaudio-microphone.sh --increase &
scroll-down = ~/.scripts/pulseaudio-microphone.sh --decrease &

;user modules end

[module/powermenu]

type = custom/menu

expand-right = true

label-open = 
label-open-foreground = ${colors.foreground}}
label-open-background = ${colors.background}
label-open-padding = 0
label-open-padding-left = 1.25
label-close = 
label-close-foreground = ${colors.foreground}
label-close-background = ${colors.background}
label-close-padding = 2
label-close-padding-right = 1
label-close-padding-left = 1.25


menu-0-0 = 
menu-0-0-foreground = ${colors.foreground}
menu-0-0-padding = 2
menu-0-0-padding-right = 1
menu-0-0-exec = poweroff
menu-0-1 = 
menu-0-1-foreground = ${colors.foreground}
menu-0-1-padding = 2
menu-0-1-padding-right = 0
menu-0-1-exec = reboot


[module/bspwm]
type = internal/bspwm
format = <label-state> <label-mode>

label-focused = 
label-focused-background = ${colors.background}
label-focused-padding = 1

label-occupied = 
label-occupied-padding = 1

label-urgent = 
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

label-empty = 
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 1

[module/filesystem]
type = internal/fs
interval = 10
fixed-values = true
mount-0 = /
label-mounted =  %percentage_used%%

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
label = %percentage%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
label = %percentage_used%%

[module/time]
type = internal/date
interval = 1
time = %H:%M
label = %time%
format = <label>

[module/pulseaudio]
type = internal/pulseaudio

label-volume =  %percentage%%
label-volume-foreground = ${root.foreground}

label-muted =  muted
label-muted-foreground = ${root.foreground}

[module/battery]
type = internal/battery

low-at = 10
full-at = 100

battery = BAT0
adapter = ACAD

poll-interval = 5

format-charging = <label-charging>
label-charging =  %percentage%%
format-discharging = <animation-discharging> <label-discharging>
label-discharging = %percentage%%

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-3 = 
animation-discharging-4 = 

animation-discharging-framerate = 1000

label-low = 
format-low = <label-low> <label-discharging>

label-full = 
format-full = <label-full> <label-discharging>

[module/backlight]
type = internal/backlight

card = amdgpu_bl1
use-actual-brightness = true
output = eDP

enable-scroll = true
format = <label>
label =  %percentage%%

[settings]
screenchange-reload = true

[global/wm]
margin-top = 0
margin-bottom = 0

