## pacman

nvidia-prime
nvidia
nvidia-utils
nvidia-settings
amd-ucode
xf86-video-amdgpu
vulkan-radeon
polybar
sxhkd
rofi
kitty
chromium
firefox
xorg-xinit
xorg-server
xorg-xset
xorg-xsetroot
feh
pulseaudio
pavucontrol
lightdm
nautilus
networkmanager
papirus-icon-theme
ttf-font-awesome
ttf-hack
xclip
python2
python3
xsettingsd
discord
lxappearance
steam
mpv
inotify-tools

## 32bit
lib32-mesa

## paru

##### git clone https://aur.archlinux.org/paru-bin.git
##### cd paru-bin
##### makepkg -si
papirus-folders
lightdm-mini-greeter
zoom
wpgtk
ttf-twemoji
spotify
spotify-adblock-git

## if isp good

dhcpcd

## lxappearance
wpg-install.sh -g

## HP Beep
https://wiki.archlinux.org/title/PC_speaker
